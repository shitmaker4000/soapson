#from http.server import *
import mysoap.parser as SOAP
import myhttp.parser as HTTP

from twisted.web import server, resource
from twisted.internet import reactor, endpoints

class ServerHandler:
    def on_cp_message(self, request, http, soap):
        pass

class ServerEvents(resource.Resource):
    isLeaf = True
    handler = None  # Польз. обработичк
    
    def __init__(self, handler):
        self.handler = handler
        
    def render_POST(self, request):
        try:
            http = HTTP.Parser().execute(request)
            soap= SOAP.Parser().execute(http.content)
            return self.handler.on_cp_message(request, http, soap)
        except Exception as e:
            print(e)
            
        request.setResponseCode(400)
        request.finish()


class Server:
    handler = None
    def __init__(self, url, port, handler):
        self.handler = ServerEvents(handler)
        factory = server.Site(self.handler)
        endpoint = endpoints.TCP4ServerEndpoint(reactor, port)
        endpoint.listen(factory)

