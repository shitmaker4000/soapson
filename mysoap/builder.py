# Разобрать заголовок. Вернуть длину, тип (по OCPP)

#VALID_ACTIONS = "/BoostNotification"
#import xml.etree.ElementTree as ETree;
# import lxml import *

from lxml import etree
from lxml.etree import QName
from lxml.builder import ElementMaker
import mysoap.namespaces as ns
import common
import uuid

class BuilderHelper:
    def normalize_action(msg_type, action):
        if msg_type == common.CALL:
            return '/' + action + 'Request'
        elif msg_type == common.CALL_RESULT:
            return '/' + action + 'Response'
        
    def normalize_name(action):
        return action[1].lower() + action[2:]
        #name = action[1:]
        #name[0] = action[2].lower()
        #return name
        
        
class ResultBuilder:
    
    def __init__(self):
        pass
    
    def execute(self, action, msg_type, msg_id, msg):

        NSMAP={'soap' : ns.SOAP_ENVELOPE }
        envelope = etree.Element(ns.add(ns.SOAP_ENVELOPE, "Envelope"), nsmap=NSMAP)
        header = etree.SubElement(envelope, ns.add(ns.SOAP_ENVELOPE, "Header"))
        act_name = BuilderHelper.normalize_action(msg_type, action)
        act  = etree.SubElement(header, "Action")
        act.attrib["xmlns"]=ns.ADDRESSING
        act.text = act_name

        new_id  = etree.SubElement(header,"MessageID")
        new_id.attrib["xmlns"]=ns.ADDRESSING
        new_id.text = str(uuid.uuid4())
        
        to  = etree.SubElement(header, "To")
        to.attrib["xmlns"]=ns.ADDRESSING
        to.text = 'http://www.w3.org/2005/08/addressing/anonymous'
        
        relates_to  = etree.SubElement(header, "RelatesTo")
        relates_to.attrib["xmlns"]=ns.ADDRESSING
        relates_to.text = msg_id

        namespace = ns.CENTRAL_SYSTEM
            
        body = etree.SubElement(envelope, ns.add(ns.SOAP_ENVELOPE, "Body"))        
        message  = etree.SubElement(body, BuilderHelper.normalize_name(act_name))
        message.attrib["xmlns"]=namespace
        
        self.build_body(message, msg)
        return etree.tostring(envelope)
        
    def build_body(self, root, msg):
        for (k,v) in msg.items():
            el = etree.SubElement(root, k)
            el.text = str(v)
            
            
            
class RequestBuilder:
    
    def __init__(self):
        pass
    
    def execute(self, chargeid, action, msg_type, msg_id, msg):
        NSMAP={'soap' : ns.SOAP_ENVELOPE }
        envelope = etree.Element(ns.add(ns.SOAP_ENVELOPE, "Envelope"), nsmap=NSMAP)
        header = etree.SubElement(envelope, ns.add(ns.SOAP_ENVELOPE, "Header"))
        act  = etree.SubElement(header, "Action")
        act.attrib["xmlns"]=ns.ADDRESSING
        act.text = "/" + action

        new_id  = etree.SubElement(header,"MessageID")
        new_id.attrib["xmlns"]=ns.ADDRESSING
        new_id.text = str(uuid.uuid4())
        
        to  = etree.SubElement(header, "To")
        to.attrib["xmlns"]=ns.ADDRESSING
        to.text = 'http://www.w3.org/2005/08/addressing/anonymous'

        cid  = etree.SubElement(header, "chargeBoxIdentity")
        cid.attrib["xmlns"]=ns.CHARGE_POINT
        cid.text = chargeid
 
        namespace = ns.CHARGE_POINT
        body = etree.SubElement(envelope, ns.add(ns.SOAP_ENVELOPE, "Body"))        
        #message  = etree.SubElement(body, BuilderHelper.normalize_action(common.CALL, action))
        message  = etree.SubElement(body, action[0].lower() + action[1:] + "Request")
        message.attrib["xmlns"]=namespace
        
        self.build_body(message, msg)
        return etree.tostring(envelope)
        
    def build_body(self, root, msg):
        for (k,v) in msg.items():
            el = etree.SubElement(root, k)
            el.text = str(v)
            

