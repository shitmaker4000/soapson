# Разобрать заголовок. Вернуть длину, тип (по OCPP)

#VALID_ACTIONS = "/BoostNotification"
#import xml.etree.ElementTree as ETree;
# import lxml import *

from lxml import etree
from lxml.etree import QName
from typing import List

import mysoap.namespaces as ns


class Header:
    chargeId = ""
    messageId = ""
    addr_from = ""
    addr_to = ""
    addr_reply_to = ""
    action = ""

    def validate(self):
        return self.messageId != "" and self.chargeId != "" and self.action != ""
    
class Message:
    header=None
    body=None
    
    def __init__(self):
        self.header=None
        self.body=None
        
class ParserHelper:
    def normalize_value(action, name, value):
        # Нормадизация значения. Конвератция из текста в нормальные типы если это возможно/необходимо
        # TODO: Все идет к тому, что нет смысла конфигурировать игнорируемые поля для каждого сообщения
        # в отдельности. Игнорить можно по имени аттрибута
        IGNORE_ATTRIBS = {
            #            'meterValuesRequest': ['connectorId']
            'bootNotificationRequest': ['chargePointSerialNumber', 'imsi', 'iccid', 'firmwareVersion'],
            'startTransactionRequest': ['idTag'],
            'stopTransactionRequest': ['idTag'],
            'authorizeRequest': ['idTag'],
            'dataTransferRequest': ['messageId', 'vendorId'],
            'statusNotificationRequest': ['vendorErrorCode'],
            'meterValuesRequest' : ['value']
        }

        # Конвертирование запрещено?
        try:
            if name in IGNORE_ATTRIBS[action]:
                return value
        except Exception:
            pass    

        # Попытаемся конвертировать число
        try:
            return float(value) if '.' in value else int(value)

        except Exception:
            pass

        return value

    def add_records(dst, src, action):
        """ добавить новые пары ключ-значения к уже разобранным """
        # Добавление нового значения в словарь. При этом:
        # выполняется нормализация значения
        # если значение встречается больше одного раза, то оно добавляется в массив
        # k - ключ в добавляемом словаре
        # v - добавляемое значение
        # nv - нормализованное значение
        # e - существующее значение
        for (k, v) in src.items():
            if(v is None):
                continue
            nv = ParserHelper.normalize_value(action, k, v)
            if(k in dst.keys()):
                e = dst[k]
                if isinstance(e, List):
                    dst[k].append(nv)
                else:
                    dst[k] = [e, nv]
            else:
                dst[k] = nv
                
    def find_node(root, name):
        KNOWN_NODES = {
            "Header": ns.SOAP_ENVELOPE,
            "chargeBoxIdentity": ns.CENTRAL_SYSTEM,
            "MessageID": ns.ADDRESSING,
            "From": ns.ADDRESSING,
            "To": ns.ADDRESSING,
            "ReplyTo": ns.ADDRESSING,
            "Action": ns.ADDRESSING,
            "Body": ns.SOAP_ENVELOPE,
        }
        if name in KNOWN_NODES:
            return root.find(ns.add(KNOWN_NODES[name], name))
        return root.find(name)
        
class Parser:

    SPECIAL_PARSER = None

    def __init__(self):
        self.SPECIAL_PARSER = {'/MeterValues': MetersParser}

    def execute(self, request):
        try:
            message =Message()
            root = etree.parse(request)
            message.header = self.parse_header(root)
            message.body = self.parse_body(root,message.header.action)
            return message

        except Exception as e:
            print("SOAPParser:" + str(e))
            
        return Message()

    def parse_header(self, root):

        header = ParserHelper.find_node(root, "Header")
        cpid = ParserHelper.find_node(header, "chargeBoxIdentity")
        message = ParserHelper.find_node(header, "MessageID")
        addr_from = ParserHelper.find_node(header, "From")
        addr_to = ParserHelper.find_node(header, "To")
        addr_reply_to = ParserHelper.find_node(header, "ReplyTo")
        action = ParserHelper.find_node(header, "Action")

        result = Header
        try:
            result.chargeId = cpid.text
            result.messageId = message.text
            result.addr_to = addr_to.text
            result.action = action.text

            # Дочерний узел "Address". Получаем доступ по индексу, т.к. там
            # толкьо один элеменгт
            result.addr_from = addr_from[0].text

            # Опциональный параметр
            result.addr_reply_to = addr_reply_to[0].text
        except:
            pass

        return result

    def parse_body(self, root, action):
        try:
            body_parser = None
            body_parser = self.SPECIAL_PARSER[action]() if action in self.SPECIAL_PARSER else DefaultBodyParser()
            node = ParserHelper.find_node(root, "Body")           
        except:
            pass
            
        return body_parser.execute(node)


class DefaultBodyParser:
    """ Дефолтный парсeр для разбора запросов SOAP.
    Рекурсивно обходит все элементы XML и составляет словарь, который повторяет структуру документа. 
    Аттрибуты XML игнорируются """

    def __init__(self):
        self.message_name = None

    def execute(self, root):
        action = QName(root[0].tag).localname
        return self.parse_node(root, action, 0)

    def parse_node(self, root, action, level):
        """ проичитать один узел """
        result = dict()
        for node in root:
            name = QName(node.tag).localname
            #print(" " * level + str(level) + " NAME:" + name)
            if len(node) > 0:
                data = self.parse_node(node, action, level + 1)
            else:
                data = node.text

            ParserHelper.add_records(result, {name: data}, action)

        return result


class MetersParser:
    """ Дефолтный парсeр для разбора запросов SOAP.
    Рекурсивно обходит все элементы XML и составляет словарь, который повторяет структуру документа. 
    Аттрибуты XML игнорируются """

    def execute(self, root):
        action = QName(root[0].tag).localname
        return self.parse_body(root, 0, action)

   
    def parse_body(self, root, level, action):
        """ читает тело сообщения, но до массива значений. Там требуется доп. обработка """
        result = dict()
        for node in root:
            name = QName(node.tag).localname

            if len(node) > 0:
                if name == 'values':
                    data = self.parse_values(node, level + 1, action)
                else:
                    data = self.parse_body(node, level + 1, action)
            else:
                data = node.text

            ParserHelper.add_records(result, {name: data}, action)

        return result

    def parse_values(self, root, level, action):
        values = []
        timestamp = None
        for node in root:

            name = QName(node.tag).localname
            if name == 'timestamp':
                timestamp = node.text
            else:
                prepared_value = {}
                ParserHelper.add_records(prepared_value, {'value': node.text}, action)
                ParserHelper.add_records(prepared_value, node.attrib, action)
                values.append(prepared_value)

        if timestamp is None:
            pair = {'value': values}
        else:
            pair = {'timestamp': timestamp, 'values': values}

        return pair
