'''
Created on Aug 21, 2018

@author: alexs
'''
from twisted.internet import reactor
from twisted.web import server
from twisted.web.client import Agent
from twisted.web.client import FileBodyProducer
from twisted.web.http_headers import Headers
from twisted.python import log
from mysoap.server import Server as SOAPServer
from mysoap.server import ServerHandler as SOAPServerHandler
from mysoap.parser import Parser as SOAPParser
from mysoap.builder import ResultBuilder as SOAPResultBuilder
from mysoap.builder import RequestBuilder as SOAPRequestBuilder


from io import BytesIO
import sys

import http.client
import myws.client as WS
import common

from autobahn.twisted.websocket import WebSocketClientProtocol, \
    WebSocketClientFactory

class CPCall:
    request=None
    http = None
    def __init__(self, request, http):
        self.request = request
        self.http = http
        
class CSCall:
    msg_id=None
    def __init__(self, msg_id):
        self.msg_id = msg_id

def modaddress(address):
    exclude = ["http://","https://"]
    l = len(address)
    trim_right = 1 if address[l-1] == '/' else 0
    for e in exclude:
        if address.find(e) >= 0:
            return address[len(e): l -  trim_right]
    return address


class Proxy(WS.ClientHandler, SOAPServerHandler):
    ws_client = None
    soap_server = None
    cp_call = None
    cs_call = None
    cp_address = None
    cp_id = None
    def __init__(self, server_url, server_port,  cs_url, cs_address, cs_port):
        self.ws_client = WS.Client(cs_address, cs_port, cs_url, self)
        self.soap_server = SOAPServer(server_url, server_port, self)
        
    def on_cs_message(self,msg_type, msg_id, data):
        
        if msg_type == common.CALL_RESULT and self.cp_call is not None:
            print("Result to CS:" + str(msg_type) + " : " +  str(msg_id) + " : " + str(data))
            # Собрать и отправить ответ 
            # TODO: Неплохо бы проверить id-сообщения
            envelope = SOAPResultBuilder().execute(self.cp_call.http.action, msg_type, msg_id, data)
  
            self.cp_call.request.setResponseCode(200)
            self.cp_call.request.setHeader('content-type', "application/soap+xml; charset=UTF-8")

            self.cp_call.request.write(envelope)
            self.cp_call.request.finish()
            print(str(envelope))
            self.cp_call = None
        elif msg_type == common.CALL:
            try:
                print("Call to CP:" + str(msg_type) + " : " +  str(msg_id) + " : " + str(data))
                action = data[0]
                payload = data[1]
                envelope = SOAPRequestBuilder().execute(self.cp_id, action, msg_type, msg_id, payload)
                print("    Action:" + action)
                print("    Full:  " + self.cp_address)
                print("    Short: " + modaddress(self.cp_address))
                self.cp_call = CSCall(msg_id)

                httpaction = "/"+action
                headers = {"Content-type": "application/soap+xml; charset=UTF-8; action={0}".format(httpaction), 
                           "Accept": "*/*", "Connection": "Keep-Alive"}
                
                # Вообще, здесь должен быть клиент на Twist...Но что-то пошло не так, а времени разбираться не было
                conn = http.client.HTTPConnection(modaddress(self.cp_address),timeout=common.CONNECT_TIMEOUT)
                conn.request("POST", "", envelope, headers)   
                response = conn.getresponse()
                answer = response.read()
                soap= SOAPParser().execute(BytesIO(answer))
                envelope = SOAPResultBuilder().execute(action, common.CALL_RESULT, msg_id, soap.body)
                self.ws_client.send_answer(msg_id, common.CALL_RESULT, soap) 
                
            except Exception as e:
                print("on_cs_message:" + str(e))

        
    
    def on_cp_message(self, request, http, soap):
        
        try:
            
            if request is not None:
                print("Call from CP: " + str(soap.body))
                self.ws_client.send(common.CALL, http, soap)     
                self.cp_call = CPCall(request, http)     
                self.cp_address = soap.header.addr_from
                self.cp_id = soap.header.chargeId
                print("    Id:" + self.cp_id)
                print("    Full:" + self.cp_address)
                
            else:
                print("Result from CP: " + str(soap.body))
                self.ws_client.send(common.CALL_RESULT, http, soap)            
        except Exception as e:
            print(e)
        return server.NOT_DONE_YET
        
def usage():
        print("Usage:")
        print("--url - local server url")
        print('--port - local server port')
        print('--remote-url - remote server url')
        print('--remote-address - remote server address')
        print('--remote-port - remote server port')
        print('Example:')
        print('<prog> --url=/ocpp --port=8080 --remote-url=steve/websocket/CentralSystemService/id1 --remote-address=192.168.56.101 --remote-port=8080')

        
if __name__ == '__main__':
    
    try:
        for arg in sys.argv:
            if arg.find('--url') >= 0:
                print(arg.split("="))
                _,url = arg.split("=")
            elif arg.find('--port') >= 0:
                _,port = arg.split("=")
            elif arg.find('--remote-url') >= 0:    
                _,remote_url=arg.split("=")
            elif arg.find('--remote-addres') >= 0:    
                _,remote_address = arg.split("=")
            elif  arg.find('--remote-port') >= 0:  
                _,remote_port = arg.split("=")
                
        # Uncomment for more log messages
        # log.startLogging(sys.stdout)    
        print("Local URL: " + url)
        print("Local port: " + port)
        print("Remote URL: " + remote_url)
        print("Remote address: " + remote_address)
        print("Remote port: " + remote_port)

        
    except Exception as e:
        print(e)
        usage()
        sys.exit()

    proxy = Proxy(url, int(port),remote_url, remote_address, int(remote_port))
    reactor.run()
