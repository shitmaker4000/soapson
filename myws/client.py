from io import StringIO
from twisted.internet import reactor
from autobahn.twisted.websocket import WebSocketClientProtocol, \
    WebSocketClientFactory
import json
import common
import uuid

class ClientHandler:
    def on_cs_message(self,msg_type, msg_id, data):
        pass
    
class ConnectionHandler:
    def on_cs_message(self,message):
        pass
    
    # Изменение статуса коннекта. Если connection = None - связь оборвана
    def on_cs_connection(self,status):
        pass

    
class Connection(WebSocketClientProtocol):

    handler = None
    
    def set_handler(self, handler):
        self.handler = handler
        
    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))
        self.handler.on_cs_connection(self, True)

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        self.handler.on_cs_message(payload.decode('utf8'))

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))
        self.connected = False
        self.handler.on_cs_connection(self, False)
        
    def send(self, message):
        self.sendMessage(message)
        return True
    
class CPCall:
    http = None
    soap = None
    def __init__(self, http, soap):
        self.http = http
        self.soap = soap
         
class Client(ConnectionHandler):
    address = None
    port = None
    url = None
    
    handler = None
    connection=None
    connected = False
    

    cp_call = None
    cs_call = None
    
    def __init__(self, address, port, url, handler):
        self.address = address
        self.port = port
        self.url = url
        self.handler=handler
        self.connected=False
        self.connection = None
        self.cp_call = None
        self.cs_call = None
        reactor.callLater(0.25, self.check_connection)
        
    def send(self, msg_type, http, soap):
        
        if msg_type == common.CALL:

            uid = str(uuid.uuid4())
            try:
                data = next(iter(soap.body.values()))
                out = [msg_type, uid, http.action, data]
            except:
                out = [msg_type, uid, http.action, {}]

            message = json.dumps(out).encode()
            print("Call to CS:" + str(message))
            self.cp_call = CPCall(http, soap)     
            self.connection.send(message)
            
    def send_answer(self, msg_id, msg_type, soap):

        if msg_type == common.CALL_RESULT:
            
            uid = msg_id
            try:
                data = next(iter(soap.body.values()))
                out = [msg_type, uid, data]
            except:
                out = [msg_type, uid, {}]

            message = json.dumps(out).encode()
            print("Result to CS:" + str(message))  
            self.connection.send(message)    
            
    # Вообще это лишняя фигня, т.к. в при неудачном коннекте twisted возвращаешт onClose...НО
    # Он так делает не всегда. И пока я ХЗ почему. Пропишем переконнект руками
    def check_connection(self):
        if self.connected == False:
            self.connect()
            reactor.callLater(common.CHECK_TIMEOUT, self.check_connection)
        else:
            reactor.callLater(common.CHECK_TIMEOUT, self.check_connection)
        
    def on_cs_message(self,message):
        try:
            data = json.load(StringIO(message))
            it = iter(data)
            msg_type = next(it)
            msg_id = next(it)
            msg_data = next(it)
            if msg_type == common.CALL_RESULT:
                self.handler.on_cs_message(common.CALL_RESULT, self.cp_call.soap.header.messageId, msg_data)
                self.cp_call = None
            elif msg_type == common.CALL:
                msg_data2 = next(it)
                self.handler.on_cs_message(common.CALL, msg_id, (msg_data, msg_data2))
                self.cs_call = None
        except Exception as e:
            print(e)
    
    # Изменение статуса коннекта. Если connection = None - связь оборвана
    def on_cs_connection(self,connection,status):
        self.connected = status
        self.connection = connection
    
    def connect(self):
        if self.connected:
           return False
       
        if self.connection is not None:
            pass    # Закрытие коннекта при переподключении
       
        url = u"ws://{0}:{1}/{2}".format(self.address, self.port, self.url)
        print("Connect to:" + url)
        connection = WebSocketClientFactory(url, protocols=['ocpp1.5'])
        connection.protocol = Connection
        connection.protocol.set_handler(connection.protocol, self)
        self.connection = connection
        reactor.connectTCP(self.address, self.port, connection)   
        return True
                