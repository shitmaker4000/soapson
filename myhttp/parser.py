# Разобрать заголовок. Вернуть длину, тип (по OCPP)

class Message:
    action = ""
    content = None
    uri = None

    def __init__(self):
        self.action = None
        self.content = None
        self.uri = None

    def validate(self):
        return self.action != ""
#       return len != 0 and self.action in VALID_ACTIONS;


class Parser:
    """ Парсер HTTP заголовка. Работает с twisted.web.http.Request """

    def __init__(self):
        pass

    def execute(self, request):

        action = ""
        # Прочитать заголовок
        try:
            # Content-Type: application/soap+xml; charset=utf-8; action="/BootNotification"
            contype = request.getHeader('content-type')
            action = contype[contype.find('action'): len(contype)-1]
            action = action.split('/')[1]

        except Exception:
            return Message()
        
        message = Message()
        message.action = action
        message.uri = str(request.uri)
        message.content = request.content

        return message
